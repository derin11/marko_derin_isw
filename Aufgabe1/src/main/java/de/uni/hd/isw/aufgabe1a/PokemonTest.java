package de.uni.hd.isw.aufgabe1a;

public class PokemonTest{
    public static void main(String[] args){

        System.out.println("Testen von nextnumber");
        System.out.println("nextnumber: " + Pokemon.nextnumber);
        //Nextnumber sollte den Wert 1 haben und sollte bei jedem Erzeugen eines Objektes
        //inkrementiert werden

        Pokemon p1=new Pokemon("Saki",Type.FIRE);

        System.out.println("Testen von getName");
        System.out.println("Name von Pokemon p1: " + p1.getName());

        System.out.println("Testen von getType");
        System.out.println("Type von Pokemon p1: " + p1.getType());

        System.out.println("Testen von getNumber");
        System.out.println("Number von Pokemon p1: " + p1.getNumber());

        System.out.println("Testen von nextnumber");
        System.out.println("nextnumber: " + Pokemon.nextnumber);
        //Number vom nächsten Pokemon muss diesen Wert haben

        Pokemon p2=new Pokemon("Loki",Type.POISON);

        System.out.println("Testen von getName");
        System.out.println("Name von Pokemon p2: " + p2.getName());

        System.out.println("Testen von getType");
        System.out.println("Type von Pokemon p2: " + p2.getType());

        System.out.println("Testen von getNumber");
        System.out.println("Number von Pokemon p2: " + p2.getNumber());

        //Test, ob die Nummer vom Pokemon p1 noch 1 ist
        System.out.println("Testen von getNumber");
        System.out.println("Number von Pokemon p1: " + p1.getNumber());

        System.out.println("Testen von nextnumber");
        System.out.println("nextnumber: " + Pokemon.nextnumber);

        System.out.println("Test von toString");
        System.out.println(p1.toString());
        System.out.println(p2.toString());
    }
}


