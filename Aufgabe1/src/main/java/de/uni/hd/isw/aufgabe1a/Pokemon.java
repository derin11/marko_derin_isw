package de.uni.hd.isw.aufgabe1a;

public class Pokemon {
    private final int number;
    public static int nextnumber=1;
    private String name;
    private Type type;

    public Pokemon(String name, Type type){
        this.name=name;
        this.type=type;
        number=nextnumber;
        nextnumber++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        /* this referenziert auf das Objekt, das aktuell aufgerufen wurde bzw. "benutzt" wird.
           In diesem Fall signalisiert die Anfügung this vor name,dass wir uns auf die Klassen-
           interne Variable name beziehen und verhindern somit eine Namenskollisiion mit der in der
           Methode enthaltenen Variable.
         */
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString(){
        return "Number= " + this.getNumber() + ", Name= " + this.getName() + ", Type= " + this.getType();
    }

    public static void main(String[] args){
        Pokemon poke;
        poke=new Pokemon("SAKI", Type.FIRE);
        /*Der Unterschied zwischen der Deklaration eines Objektes und der Erzeugung des Objektes besteht
          darin, dass bei der Deklarartion eine Variable vom Typ Pokemon erstellt wird. Dieser
          Variable wird aber noch nichts zugewiesen und daher auch noch kein Objekt erzeugt. Erst,
          sobald wird der Variable etwas zuweisen, dann wird das Objekt erzeugt bzw. initialisiert.
         */

        System.out.println(poke.toString());
        Pokemon mon=new Pokemon("Loki", Type.POISON);
        System.out.println(mon.toString());
    }
}